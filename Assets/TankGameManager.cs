﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.IO;
using UnityEngine.SceneManagement;

public class TankGameManager : MonoBehaviourPunCallbacks
{

    public static TankGameManager Instance;

    [SerializeField]private Transform[] spawnPoints;
    [SerializeField] private Material[] playercolors;

    // Start is called before the first frame update
    void Start()
    {
        //make sure there is only one instance of this object alive
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Debug.Log("instance already exists, destroying this object");
            Destroy(this);
        }

        CreatPlayer();
        CreatZombie();
    }
    /// <summary>
    /// method for creating the player from a prefab in the server 
    /// </summary>
    private void CreatPlayer()
    {
        if (TankController.LocalPlayerInstance == null)
        {
            Vector3 spawnPos = spawnPoints.Length > 0 ? spawnPoints[Random.Range(0, spawnPoints.Length - 1)].position : Vector3.zero;

            Debug.Log("Creating Player");
            PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonPlayer"), spawnPos, Quaternion.identity);
        }else
            Debug.LogFormat("Ignoring scene load for {0}", SceneManagerHelper.ActiveSceneName);

        

    }

    /// <summary>
    /// method for iunstantiating a zombie in the server 
    /// </summary>

    private void CreatZombie()
    {
        Vector3 spawnPos = spawnPoints.Length > 0 ? spawnPoints[Random.Range(0, spawnPoints.Length - 1)].position : Vector3.zero;

        Debug.Log("Creating Zombie");
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonZombie"), spawnPos, Quaternion.identity);
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene(0);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
