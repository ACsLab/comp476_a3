﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Player Owner { get; private set; }
    [SerializeField] private float bulletSpeed = 50f; 
    [SerializeField]private float DefaultDamage = 0.5f;
    public float Damage = 0.5f;
    private bool bounce;
    public bool Bounce
    {
        get { return bounce; }
        set
        {

            gameObject.GetComponent<SphereCollider>().isTrigger = value ? false : true;
                
            bounce = value;
        }
    }
    private new Rigidbody rigidbody;


    public void ResetToDefault()
    {
        Damage = DefaultDamage;
        Bounce = false;
    }

    public void Start()
    {
        Damage = DefaultDamage;
        
        Destroy(this.gameObject, 2.0f);
    }

    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision!!");
        if (collision.gameObject.tag == "Wall")
            collision.gameObject.SetActive(false);
        
        HandleBounce(collision.contacts[0].normal);
        
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Triggered!!");
        if (other.tag == "Wall")
            other.gameObject.SetActive(false);
  
        Destroy(this.gameObject);
    }

    private void HandleBounce(Vector3 contactNormal)
    {
        Vector3 vel = rigidbody.velocity;

        Vector3 r = vel - 2 * (Vector3.Dot(vel, contactNormal)) * contactNormal;

        rigidbody.velocity = r;

        transform.rotation = Quaternion.LookRotation(rigidbody.velocity);

        if (rigidbody.velocity == Vector3.zero)
            Destroy(this.gameObject);

    }

    public void InitializeBullet(Player owner, Vector3 originalDirection, float lag)
    {
        Owner = owner;

        transform.forward = originalDirection;

        rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = originalDirection * bulletSpeed;
        rigidbody.position += rigidbody.velocity * lag;
    }
}
