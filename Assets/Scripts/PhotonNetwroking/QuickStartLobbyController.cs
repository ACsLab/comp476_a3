﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class QuickStartLobbyController : MonoBehaviourPunCallbacks
{

    [SerializeField]
    private GameObject StartButton;
    [SerializeField]
    private GameObject cancelButton;
    [SerializeField]
    private int roomSize;
    [SerializeField]
    private InputField Input;


    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        StartButton.SetActive(true);
    }

    public void ConnectToServer()//connected to the start button action 
    {
        StartButton.SetActive(false);
        cancelButton.SetActive(true);
        PhotonNetwork.NickName = Input.text;
        PlayerPrefs.SetString("PlayerName", Input.text);
        PhotonNetwork.JoinRandomRoom();
        Debug.Log("Started! attempting to join random room.");


    }
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to join random room");
        CreateRoom();
    }

    private void CreateRoom()
    {
        Debug.Log("Creating new room");
        int randomRoomNumber = Random.Range(0, 10000);
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true,MaxPlayers = (byte)roomSize};
        PhotonNetwork.CreateRoom("Room" + randomRoomNumber, roomOps);
        Debug.Log($"created random room number {randomRoomNumber}");

    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to create room... trying again");
        CreateRoom();
    }

    public void Cancel()
    {
        cancelButton.SetActive(false);
        StartButton.SetActive(true);

        PhotonNetwork.LeaveRoom();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
