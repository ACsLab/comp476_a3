﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class GameSetupController : MonoBehaviour
{
    [SerializeField] private Transform[] spawnPoints;
    
    // Start is called before the first frame update
    void Start()
    {
        CreatPlayer();
    }

    private void CreatPlayer()
    {

        Vector3 spawnPos = spawnPoints.Length > 0 ? spawnPoints[Random.Range(0, spawnPoints.Length - 1)].position : Vector3.zero;

        Debug.Log("Creating Player");
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs","PhotonPlayer"), spawnPos, Quaternion.identity);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
