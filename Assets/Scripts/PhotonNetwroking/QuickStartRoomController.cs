﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class QuickStartRoomController : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private int multiplayerSceneIndex; //number for the build index to the mutiplayer scene

    public override void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    public override void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public override void OnJoinedRoom()//callback function for when we succesfully joined a room
    {
        Debug.Log("Joined Room");
        StartGame();


    }

    private void StartGame()//function for loading into the multiplayer scene 
    {
        if (PhotonNetwork.IsMasterClient)
        {
            Debug.Log("current client is Master Client, Starting Game");
            PhotonNetwork.LoadLevel(multiplayerSceneIndex);//because of autosync all players will also be loaded into the multiplayer scene

        }
    }


   
}
