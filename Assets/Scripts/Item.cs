﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct PowerUp
{
    public enum powerupType { ExtraDamage, Bounce, NoReloade, NONE}

    public float damageModifier;

    public powerupType ptype;

    
    public PowerUp(powerupType type)
    {
        ptype = type;

        damageModifier = 0;
    }

    public PowerUp(powerupType type, float dmodifier)
    {
        ptype = type;

        damageModifier = ptype == powerupType.ExtraDamage? dmodifier: 0;
    }

}


public class Item : MonoBehaviour
{

    [SerializeField] public PowerUp powerUp;
    [SerializeField] private float timeout = 10.0f;


}
