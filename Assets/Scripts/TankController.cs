﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using Photon.Pun;
using System;

public class TankController : MonoBehaviourPunCallbacks, IPunObservable
{
    

    [SerializeField]private float movementSpeed = 0.25f;
    [SerializeField]private float rotationSpeed = 5.0f;
   
    [SerializeField]private GameObject BulletPrefab;
    [SerializeField]private float Health = 10f;
    [SerializeField]private float ReloadTime = 0.5f;
    [SerializeField]private float PowerUpTime = 5.0f;

    private float PowerUpTimer = 0.0f;
    private float shootingTimer = 0.0f;


    public static TankController LocalPlayerInstance;
    private CharacterController charCont;
    private PowerUp powerup = new PowerUp(PowerUp.powerupType.NONE);

    private bool isFiring = false;

    private void Awake()
    {   
        
        // #Important
        // used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
        
        charCont = GetComponent<CharacterController>();
        if (BulletPrefab == null)
            Debug.LogError("<Color=Red><a>Missing</a></Color> Beams Reference.", this);

        Physics.IgnoreLayerCollision(8, 8);
        if (photonView.IsMine)
        {
            
            TankController.LocalPlayerInstance = this;
        }
        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        DontDestroyOnLoad(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        CameraController _cameraWork = this.gameObject.GetComponent<CameraController>();


        if (_cameraWork != null)
        {
            if (photonView.IsMine)
            {
                _cameraWork.OnStartFollowing();
            }
        }
        else
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> CameraWork Component on playerPrefab.", this);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
        {
            return;
        }

        if (photonView.IsMine)
        {
            HandleMove();
            CheckMouseInputs();

           

            if (isFiring && shootingTimer <= 0.0)
            {
                shootingTimer = ReloadTime;

                photonView.RPC("Fire", RpcTarget.AllViaServer, transform.position + new Vector3(0,3,0), transform.rotation);
            }

            if (shootingTimer > 0.0f)
            {
                shootingTimer -= Time.deltaTime;
            }

            if (PowerUpTimer > 0f)
            {
                PowerUpTimer -= Time.deltaTime;
            }
            else
                BulletPrefab.GetComponent<Bullet>().ResetToDefault();


            if (Health <= 0f)
            {
                TankGameManager.Instance.LeaveRoom();
            }
        }

        


    }
    /// <summary>
    /// the method that decides what to do with the power up that was picked up 
    /// </summary>
    private void HandlePowerup()
    {


        PowerUpTimer = PowerUpTime;

        if (powerup.ptype == PowerUp.powerupType.ExtraDamage)
        {
            BulletPrefab.GetComponent<Bullet>().Damage = powerup.damageModifier;
            powerup = new PowerUp(PowerUp.powerupType.NONE);

            return;
        }
        
        if (powerup.ptype == PowerUp.powerupType.Bounce)
        {
            BulletPrefab.GetComponent<Bullet>().Bounce = true;
            powerup = new PowerUp(PowerUp.powerupType.NONE);

            return;
        }

        


    }
    /// <summary>
    /// a simple move method for the player 
    /// </summary>
    private void HandleMove()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        float mousex = Input.GetAxis("Mouse X");

        if (mousex < 0 || h < 0)
            transform.Rotate(Vector3.up * -rotationSpeed);
        if (mousex > 0 || h > 0)
            transform.Rotate(Vector3.up * rotationSpeed);

        if (!charCont.isGrounded)
            charCont.Move(-transform.up * 9.8f);
        charCont.Move(transform.forward * movementSpeed * v);
        

    }
    /// <summary>
    /// checking if the player has fired the gun
    /// </summary>
    private void CheckMouseInputs()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (!isFiring)
            {
                isFiring = true;
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if (isFiring)
            {
                isFiring = false;
            }
        }
    }
    /// <summary>
    /// this method takes in the position to spawn the bullet in and its rotation 
    /// </summary>
    /// <param name="position"></param>
    /// <param name="rotation"></param>
    /// <param name="info"></param>
    [PunRPC]public void Fire(Vector3 position, Quaternion rotation, PhotonMessageInfo info)
    {
        float lag = (float)(PhotonNetwork.Time - info.SentServerTime);
        GameObject bullet;

        //initializes the buulet 
        bullet = Instantiate(BulletPrefab, position, Quaternion.identity) as GameObject;
        bullet.GetComponent<Bullet>().InitializeBullet(photonView.Owner, (rotation * Vector3.forward), Mathf.Abs(lag));


        
    }

    /// <summary>
    /// MonoBehaviour method called when the Collider 'other' enters the trigger.
    /// Affect Health of the Player if the collider is a bullet
    /// </summary>
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log($"{photonView.ViewID} getting shot at, health: {photonView.gameObject.GetComponent<TankController>().Health}");

        if (!photonView.IsMine)
        {
            return;
        }

        if (collision.gameObject.tag == "Item")
        {
            powerup = collision.gameObject.GetComponent<Item>().powerUp;
            Debug.Log($"picking up item {powerup.ptype}");
            HandlePowerup();
        }
        // We are only interested in Beamers
        // we should be using tags but for the sake of distribution, let's simply check by name.
        if (collision.gameObject.tag != "Bullet")
        {
            return;
        }
        Debug.Log($"Subtracting health");
        Health -= BulletPrefab.GetComponent<Bullet>().Damage;

    }

    /// <summary>
    /// MonoBehaviour method called when the Collider 'other' enters the trigger.
    /// Affect Health of the Player if the collider is a bullet
    /// </summary>
    void OnTriggerEnter(Collider other)
    {

        Debug.Log($"{photonView.ViewID} getting shot at, health: {photonView.gameObject.GetComponent<TankController>().Health}");

        if (!photonView.IsMine)
        {
            return;
        }

        if (other.tag == "Item")
        {
            
            powerup = other.GetComponent<Item>().powerUp;
            Debug.Log($"picking up item {powerup.ptype}");
            HandlePowerup();
        }
        // We are only interested in Beamers
        // we should be using tags but for the sake of distribution, let's simply check by name.
        if (other.tag != "Bullet")
        {
            return;
        }
        Debug.Log($"Subtracting health");
        Health -= BulletPrefab.GetComponent<Bullet>().Damage;
    }
    /// <summary>
    /// MonoBehaviour method called once per frame for every Collider 'other' that is touching the trigger.
    /// We're going to affect health while the beams are touching the player
    /// </summary>
    /// <param name="other">Other.</param>
    void OnTriggerStay(Collider other)
    {
        // we dont' do anything if we are not the local player.
        if (!photonView.IsMine)
        {
            return;
        }
        // We are only interested in Beams
        
        

        if (other.tag != "Bullet")
        {
            return;
        }
        // we slowly affect health when beam is constantly hitting us, so player has to move to prevent death.
        Health -= 0.5f * Time.deltaTime;
    }


    /// <summary>
    /// important method for netwrked player to send and receive information 
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="info"></param>
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            Debug.Log($"Sending over data");
            
            stream.SendNext(isFiring);
            stream.SendNext(Health);
        }
        else
        {

            // Network player, receive data
            this.isFiring = (bool)stream.ReceiveNext();
            this.Health = (float)stream.ReceiveNext();
        }


    }



}



