﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using Photon.Pun;
using System;

public class ZombieTankController : MonoBehaviourPunCallbacks, IPunObservable
{


    [SerializeField] private float movementSpeed = 0.25f;
    [SerializeField] private float rotationSpeed = 5.0f;
    [SerializeField] private float attackDistance = 10f;
    [SerializeField] private GameObject BulletPrefab;
    [SerializeField] private float Health = 1f;
    [SerializeField] private float MaxVelocity = 1f;
    [SerializeField] private float arrive_radius = 5f;
    [SerializeField] private float ReloadTime = 0.5f;

    private float shootingTimer = 0.0f;
    public static ZombieTankController LocalPlayerInstance;
    private CharacterController charCont;
    public List<GameObject> humanTargets;
    private int targetindex =-1;
    

    private bool isFiring = false;

    private void Awake()
    {

        // #Important
        // used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized

        charCont = GetComponent<CharacterController>();
        if (BulletPrefab == null)
            Debug.LogError("<Color=Red><a>Missing</a></Color> Beams Reference.", this);


        Physics.IgnoreLayerCollision(8, 8);
        if (photonView.IsMine)
        {

            ZombieTankController.LocalPlayerInstance = this;
        }
        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.
        DontDestroyOnLoad(this);
    }

    // Start is called before the first frame update
    void Start()
    {
       

        humanTargets.Add(GameObject.Find("PhotonPlayer(Clone)"));
        Debug.Log($"humans detected {humanTargets.Count}");
        
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
        {
            if (targetindex == -1)
                targetindex = AquireNewTarget();

            CheckForObstruction();
            HandleMove();

            //check if target is in firing range
            //if yes fire
            if (Vector3.Distance(transform.position, humanTargets[targetindex].transform.position) <= attackDistance
                && shootingTimer <= 0.0)
            {
                shootingTimer = ReloadTime;
                photonView.RPC("Fire", RpcTarget.AllViaServer, transform.position + new Vector3(0, 3, 0), transform.rotation);

            }

            if (shootingTimer > 0.0f)
            {
                shootingTimer -= Time.deltaTime;
            }
        }
        

    }

    private int AquireNewTarget()
    {
        return UnityEngine.Random.Range(0, humanTargets.Count-1);
    }

    //determin the new velocity for the tank
    private Vector3 GetSeekVelocity(Vector3 currentposition, Vector3 target)
    {

        Vector3 posdiff = target - currentposition;
        Vector3 absposdiff = new Vector3(Math.Abs(posdiff.x), Math.Abs(posdiff.y), Math.Abs(posdiff.z));
        float theta = 1e-5f;
        Vector3 am = new Vector3(posdiff.x / (absposdiff.x + theta), 0, posdiff.z / (absposdiff.z + theta));

        

        return charCont.velocity + movementSpeed * am * Time.deltaTime;
    }

    /*
     *Steering seek
     * move towards a target using the steering seek calculation
     * if bool flee is set to true the entity will face run away from the tagger
     */
    void Seek()
    {

        //if (Vector3.Distance(transform.position, humanTargets[targetindex].transform.position) <= arrive_radius)
        //    Arrive();

        Vector3 currentPosition = this.transform.position;
        Vector3 targetPosition = humanTargets[targetindex].transform.position;
        Vector3 target_dir = targetPosition - currentPosition;
        target_dir = new Vector3(target_dir.x, 0, target_dir.z);
        //Vector3 currentSeekVelocity = GetSeekVelocity(currentPosition, targetPosition);
        //Debug.Log($"Zombie current seek velocity {charCont.velocity}");

        charCont.Move(target_dir * movementSpeed * Time.deltaTime);

        UpdateOrientation();



    }

    private void Arrive()
    {
        throw new NotImplementedException();
    }

    private void UpdateOrientation()
    {
        Vector3 currentPosition = transform.position;
        
        Quaternion targetRotation = charCont.velocity == Vector3.zero? Quaternion.LookRotation(humanTargets[targetindex].transform.position-transform.position): Quaternion.LookRotation(charCont.velocity);

        float strength = Mathf.Min((float)(MaxVelocity * Time.deltaTime), 1.0f);

        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, strength);

    }

    private void HandleMove()
    {
        //float h = Input.GetAxis("Horizontal");
        //float v = Input.GetAxis("Vertical");

        //float mousex = Input.GetAxis("Mouse X");

        //if (mousex < 0 || h < 0)
        //    transform.Rotate(Vector3.up * -rotationSpeed);
        //if (mousex > 0 || h > 0)
        //    transform.Rotate(Vector3.up * rotationSpeed);


        //charCont.Move(transform.forward * movementSpeed * v);
        if (!charCont.isGrounded)
            charCont.Move(-transform.up * 9.8f);
        Seek();

    }
    //check if there is an obstruction infront of the zombie
    //if there is shoot at it to destroy it 
    private void CheckForObstruction()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + new Vector3(0, 3, 0), transform.forward, out hit, 10f) && shootingTimer <= 0.0f)
        {
            shootingTimer = ReloadTime;
            photonView.RPC("Fire", RpcTarget.AllViaServer, transform.position + new Vector3(0, 3, 0), transform.rotation);

        }
    }
    private void CheckMouseInputs()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (!isFiring)
            {
                isFiring = true;
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if (isFiring)
            {
                isFiring = false;
            }
        }
    }

    [PunRPC]
    public void Fire(Vector3 position, Quaternion rotation, PhotonMessageInfo info)
    {
        float lag = (float)(PhotonNetwork.Time - info.SentServerTime);
        GameObject bullet;

        /** Use this if you want to fire one bullet at a time **/
        bullet = Instantiate(BulletPrefab, position, Quaternion.identity) as GameObject;
        bullet.GetComponent<Bullet>().InitializeBullet(photonView.Owner, (rotation * Vector3.forward), Mathf.Abs(lag));



    }

    /// <summary>
    /// if the zombie is hit by another player he will retarget to the new player 
    /// </summary>
    void OnTriggerEnter(Collider other)
    {

        Debug.Log("Zombies cannot die!");
    }
    
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            Debug.Log($"Sending over data");

            stream.SendNext(isFiring);
            stream.SendNext(Health);
        }
        else
        {

            // Network player, receive data
            this.isFiring = (bool)stream.ReceiveNext();
            this.Health = (float)stream.ReceiveNext();
        }


    }



}